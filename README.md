
### Part 1
```

Imp classes 
 - AuthorizationServerConfig
 - WebSecurityConfig
 
Imp Annotation
 - @EnableAuthorizationServer
 - @EnableResourceServer -- without this we can't access controllers of auth server even with valid token
 
git
 - repo - https://gitlab.com/spring-51/security/youtube/authorization-server/spring-security-auth-server.git
 - branch -  part 1
 - instructor 
   - repo - https://github.com/lspil/youtubechannel/tree/master/springsecurity101authserver
   - branch - master
   
```

#### request
```
GET 127.0.0.1:8081/oauth/token?scope=write&grant_type=password&username=<user's username>&password=<user's password>
with this pass clintId and secret as basic auth 
where 
  username is clientId -- this username is NOT SAME as as username in url
  password is secret -- this password is NOT SAME as as password in url
  refer - https://www.youtube.com/watch?v=IcB8qrDnfA8&t=1032s - part 1
  

```

#### response
```
{
    "access_token": "8510e495-06ac-468e-bd9b-8e4b55fb6901",
    "token_type": "bearer",
    "refresh_token": "de7ceabf-366b-431d-bc9d-e2663df034a0",
    "expires_in": 43199,
    "scope": "write"
}

```
##############################################################################################################
### Part 2

```
Imp classes
- AuthorizationServerConfig
- ResourceServerConfig
- pom.xml

git
- repo - https://gitlab.com/spring-51/security/youtube/authorization-server/spring-security-auth-server.git
- branch -  part2
- instructor
    - repo - https://github.com/lspil/youtubechannel/blob/master/springsecurityauth102
    - branch - master

```

#### request
```
GET 127.0.0.1:8081/oauth/token?scope=write&grant_type=password&username=<user's username>&password=<user's password>
with this pass clintId and secret as basic auth
where
username is clientId -- this username is NOT SAME as as username in url
password is secret -- this password is NOT SAME as as password in url
refer - https://www.youtube.com/watch?v=IcB8qrDnfA8&t=1032s - part 1

```

#### response
```
{
"access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjE1NDc2MTQsInVzZXJfbmFtZSI6ImFkbWluIiwiYXV0aG9yaXRpZXMiOlsiYWRtaW4iXSwianRpIjoiYTYwMTQ1ZjUtYTY0Yi00Nzk3LWE4MzctMzM2MmU5NjJhYWZmIiwiY2xpZW50X2lkIjoiY2xpZW50SWQiLCJzY29wZSI6WyJ3cml0ZSJdfQ.Ip5FAgmaqypWLTvx0DtdV2qzppF9xDM1M0emtiK_tAs",
"token_type": "bearer",
"refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJhZG1pbiIsInNjb3BlIjpbIndyaXRlIl0sImF0aSI6ImE2MDE0NWY1LWE2NGItNDc5Ny1hODM3LTMzNjJlOTYyYWFmZiIsImV4cCI6MTYyNDA5NjQxNCwiYXV0aG9yaXRpZXMiOlsiYWRtaW4iXSwianRpIjoiYThmZThhMjAtYjdmYy00MDlkLTkxMzMtNDZiZjZmZmJjMjJkIiwiY2xpZW50X2lkIjoiY2xpZW50SWQifQ.xpXLdWc3yfzml3cvtr6S_Vvq2rh1d3kqKoJjMxcXl8s",
"expires_in": 43199,
"scope": "write",
"jti": "a60145f5-a64b-4797-a837-3362e962aaff"
}

-- in order to access any resource  within auth server pass  above "access_token" as
Authorization : Bearer <access_token>

try this with GET 127.0.0.1:8081/test
```
