package com.poc.springsecurity.tokenenhancer;

import com.poc.springsecurity.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class CustomTokenEnhancer  implements TokenEnhancer {

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        DefaultOAuth2AccessToken defaultOAuth2AccessToken = (DefaultOAuth2AccessToken) oAuth2AccessToken;

        Long rightNowInMS = System.currentTimeMillis();

        // 5 mins
        defaultOAuth2AccessToken.setExpiration(new Date(rightNowInMS + 5*60*1000));
        Map<String, Object> customClaims = new HashMap<>();
        customClaims.put("userId",myUserDetailsService.getUserId());
        customClaims.put("iat", new Date(rightNowInMS)); // giving incorrect date on jwt.io

        defaultOAuth2AccessToken.setAdditionalInformation(customClaims);

        return oAuth2AccessToken;
    }
}
