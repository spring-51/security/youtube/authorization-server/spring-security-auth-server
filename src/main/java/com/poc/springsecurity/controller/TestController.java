package com.poc.springsecurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Generally we dont have resources in Auth Server.
 * Controllers are not part or resource server.
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {

    @GetMapping
    public String test(){
        return "TestController -> test from auth server";
    }
}
