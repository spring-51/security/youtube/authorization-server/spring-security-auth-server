package com.poc.springsecurity.config;

import com.poc.springsecurity.tokenenhancer.CustomTokenEnhancer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.Arrays;

/**
 * This class is used for configuring of
 * * User Authentication(using user details service + authentication providers)
 * * User Authorization
 */
/**
 This class is used for configuring of
 * * Registered Client Authentication( what classes like UserDetailsService we have for this ??)
 * * Registered Client Authorization
 *
 *
 * AuthorizationServerConfigurerAdapter is deprecated in 2.5.1.RELEASE version of
 * "spring-security-oauth2" artifact
 * what is the alternative, since its deprecated ?
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    /**
     * this is coming from WebSecurityConfig
     * we have declared @Bean in WebSecurityConfig
     */
    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * this is used to add custom claims to jwt
     */
    @Autowired
    private CustomTokenEnhancer tokenEnhancer;

    /**
     * it contains list of registered client which is using
     * this auth server
     * @param clients
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("clientId")
                .secret("secret")
                .authorizedGrantTypes("password","refresh_token","authorization_token") // what's this ?
                .scopes("write") // what's this ? - its what the client is allowed to do
                ;
    }

    // MANDATORY // Spring does not autowire authenticationManager to Client verification
    // authenticationManager is same what we used for User verification
    /**
     * to customized authorization endpoint, (?)
     * @param endpoints
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(tokenEnhancer, jwtAccessTokenConverter()));
        endpoints.authenticationManager(authenticationManager)
                .tokenEnhancer(tokenEnhancerChain)
                .tokenStore(tokenStore())
                .accessTokenConverter(jwtAccessTokenConverter())
        ;
    }

    /**
     *
     *
     * @return
     */
    @Bean
    public TokenStore tokenStore(){
        /**
         * it create token using UUID
         * NOT A GOOD PRACTICE to use because
         * * it store token in memory , every time server re-start we will lose all tokens
         * * it just UUID we can add payload (claims to this)
         * * UUID can't be signed with a secret , hence it's vulnerable to hack
         *
         * It's used for demo
         */
        // InMemoryTokenStore

        /**
         * NOT A GOOD PRACTICE to use because
         * it's same as InMemoryTokenStore
         * it just store UUID in table so that we wil not lose tokens for every restart
         * BUT SINCE its UUID its vulnerable to hack
         */
        // JdbcTokenStore

        /**
         * GOOD PRACTICE to use because
         * * we can add payload (claims to this)
         * * it's signed with a secret , hence it's less-vulnerable to hack
         */
        JwtTokenStore tokenStore = new JwtTokenStore(jwtAccessTokenConverter());

        return tokenStore;
    }

    @Bean
    public JwtAccessTokenConverter  jwtAccessTokenConverter(){
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey("12345"); // secret signing key
        return converter;
    }

    /**
     * to whitelist oauth2/token api
     * eg. {baseUrl}/oauth/check_token?token=<token>
     * @param security
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.checkTokenAccess("isAuthenticated()");
    }
}
