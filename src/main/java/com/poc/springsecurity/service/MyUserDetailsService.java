package com.poc.springsecurity.service;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MyUserDetailsService {

    public String getUserId(){
        return UUID.randomUUID().toString();
    }
}
